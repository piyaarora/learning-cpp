#include <iostream>
using namespace std;

struct node
{
    int data;
    node *next;
};

node *head;

//declarations
void insertAtBeginning(int val);
void insertAtEnd(int val);
void insertAtNth(int val, int pos);
void iterativeReverse();
// void recursiveReverse();
void deleteStart();
void deleteVal(int pos);
void print();

int main()
{
    head = NULL;
    cout << "list after insertion in the beginning:" << endl;
    insertAtBeginning(4);
    insertAtBeginning(3);
    insertAtBeginning(2);
    insertAtBeginning(1);
    print();

    cout << "reverse a list" << endl;
    iterativeReverse();
    print();

    cout << "list after insertion at the end:" << endl;
    insertAtEnd(6);
    print();

    cout << "list after insertion at the particular:" << endl;
    insertAtNth(5, 5);
    print();

    cout << "list after deletion in the start:" << endl;
    deleteStart();
    print();

    cout << "list after deleting element at particular position:" << endl;
    deleteVal(3);
    print();
    return 0;
};

void insertAtBeginning(int val)
{
    node *temp = new node();
    temp->data = val;
    temp->next = head;

    head = temp;
}

void insertAtEnd(int val)
{
    node *temp = new node();
    temp->data = val;
    temp->next = NULL;
    if (head == NULL)
    {
        head = temp;
    }
    else
    {
        node *temp1 = head;
        while (temp1->next != NULL)
        {
            temp1 = temp1->next;
        }
        temp1->next = temp;
    }
}
void insertAtNth(int val, int pos)
{
    node *temp = new node();
    temp->data = val;
    temp->next = NULL;
    if (head == NULL)
    {
        head = temp;
    }
    else
    {
        node *temp1 = head;
        for (int i = 0; i < pos - 2; i++)
        {
            temp1 = temp1->next;
        }
        temp->next = temp1->next;
        temp1->next = temp;
    }
}

void iterativeReverse()
{
    node *prev, *current, *nxt;
    prev = NULL,
    nxt = NULL;
    current = head;
    while (current != NULL)
    {
        nxt = current->next;  //pointed to second node
        current->next = prev; //first node ka next = null
        prev = current;       //prev first node ka address
        current = nxt;        //head points to node 2
    }
    head = prev;
}

void deleteStart()
{
    node *temp2 = head;
    if (head == NULL)
    {
        delete (temp2);
        cout << "list is empty";
    }
    head = temp2->next;
}
void deleteVal(int pos)
{
    node *temp = head;
    node *temp1;
    if (head == NULL)
    {
        delete (temp);
    }
    else
    {
        for (int i = 0; i < pos - 2; i++)
        {
            temp = temp->next;
        }
        temp1 = temp->next;
        temp->next = temp1->next;
        delete (temp1);
    }
}
void print()
{
    node *temp1 = head;
    if (temp1 == NULL)
    {
        cout << "List is empty";
    }
    else
    {
        while (temp1 != NULL)
        {
            cout << temp1->data << "\t";
            temp1 = temp1->next;
        }
        cout << endl;
    }
}
